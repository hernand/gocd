# gocd

## Getting started
1-Add the GoCD helm chart repository:
```
helm repo add gocd https://gocd.github.io/helm-chart
helm repo update
```
2-Run the install command:
For Helm v3:
```
kubectl create ns gocd 
helm install gocd gocd/gocd --namespace gocd
```
3-Port forward
```
kubectl port-forward -n gocd gocd-server-d9d967685-9fhj2 8153:8153
```

```
http://localhost:8153/go/pipelines#!/
```
